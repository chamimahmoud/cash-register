﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Caisse
{
    class CalculPrix
    {
        public double PrixProduit(double poids, double prix)
        {
            return poids * prix;
        }

        public double PrixGlobal(double prixGlobal, double prixNV)
        {
//            prixGlobal = 0;
            prixGlobal += prixNV;
            return prixGlobal;
        }

        public double tva(double prixGlobal)
        {
            return prixGlobal * 0.2;
        }
    }
}
