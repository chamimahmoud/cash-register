﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Caisse
{

    class Class1
    {
        private Dictionary<string, string> produitPrixList;

        public Dictionary<string, string> ProduitPrixList 
        { 
            get => produitPrixList; 
            set => produitPrixList = value; 
        }

        public string ProduitPrix(string produit)
        {
            string prix;
            ProduitPrixList.TryGetValue(produit, out prix);
            return prix;
        }
    }
}

