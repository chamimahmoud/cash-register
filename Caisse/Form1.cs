﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Caisse
{
    public partial class Form1 : Form
    {
        private CalculPrix prix;
        private Class1 produit;
        private double resultat = 0;
        private double resultat1 = 0;
        private ticket stock;
        private DataImport data;
        private Dictionary<string,string> newlistProduct;
        private int count = 0;

        public Form1()
        {
            InitializeComponent();
            prix = new CalculPrix();
            produit = new Class1();
            stock = new ticket();
            data = new DataImport();
            affichageTextBox.Text = ""; 
        }

        private void NPButton_Click(object sender, EventArgs e)
        {
            stock.Log("-------------------------------- \n" + "Primeur de la côte \n" + "Avenue de beaurivage\n" + "64200 Biarritz \n \n" + "le " + DateTime.Now.ToString("MM/dd/yyyy") + "\n" + "à " + DateTime.Now.ToString("HH:mm") + "\n");
            DateTime dt = DateTime.Now;
            poidsTextBox.Clear();
            affichageTextBox.Clear();
            prixTextBox.Clear();
            prixGlobalTextBox.Clear();
            comboBox1.Text = "Choisissez votre produit";
            count = 0;

            label1.Show();
            label2.Show();
            comboBox1.Show();
            poidsTextBox.Show();
            label3.Show();
            label4.Show();
            label5.Show();
            label6.Show();
            label7.Show();
            prixTextBox.Show();
            prixGlobalTextBox.Show();
            validerButton.Show();
            voirButton.Show();
            num1.Show();
            num2.Show();
            num3.Show();
            num4.Show();
            num5.Show();
            num6.Show();
            num7.Show();
            num8.Show();
            num9.Show();
            num0.Show();
            virgule.Show();
            eff.Show();
            affichageTextBox.Show();
        }

        private void validerButton_Click(object sender, EventArgs e)
        {
            string res;
            res = produit.ProduitPrix(comboBox1.Text);
            
            try
            {
                resultat = prix.PrixProduit(
                Double.Parse(poidsTextBox.Text),
                Double.Parse(res)
                );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            prixTextBox.Text = resultat.ToString();

            if (poidsTextBox.Text != "" && comboBox1.Text != "Choisissez votre produit") 
            {
                try
                {
                    resultat1 = prix.PrixGlobal(resultat1, resultat);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            prixGlobalTextBox.Text = resultat1.ToString();

            if (poidsTextBox.Text != "" && comboBox1.Text != "Choisissez votre produit")
            {
                affichageTextBox.Text += (comboBox1.Text + " - " + poidsTextBox.Text + "kg : " + prixTextBox.Text + "€ \r\n");
            }
            else
            {
                MessageBox.Show("Veuillez remplir tous les paramètres.");
            }

            if(comboBox1.Text != produit.ProduitPrix(comboBox1.Text))
            {
                poidsTextBox.Clear();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Text = "Choisissez votre produit";
        }

        private void dataButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            produit.ProduitPrixList = data.ImportProduitPrix(openFileDialog1.FileName);

            newlistProduct = produit.ProduitPrixList;
            foreach (KeyValuePair<string, string> element in newlistProduct)
            {
                comboBox1.Items.Add(element.Key);
            }
        }
        
        private void voirButton_Click(object sender, EventArgs e)
        {
            count++;
            if (count > 1)
            {
                stock.Log("-------------------------------- \n" + "Primeur de la côte \n" + "Avenue de beaurivage\n" + "64200 Biarritz \n \n" + "le " + DateTime.Now.ToString("MM/dd/yyyy") + "\n" + "à " + DateTime.Now.ToString("HH:mm") + "\n");
            }

            stock.Log(affichageTextBox.Text);
            stock.Log("TOTAL TTC : " + prixGlobalTextBox.Text +" €");
           
            try
            {
                resultat1 = prix.tva(double.Parse(prixGlobalTextBox.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            string tva = resultat1.ToString();
            stock.Log("TVA : " + tva + " €");
            stock.Log("\n" + "Merci de votre visite et...\n" + "... Gardez la pêche! \n" + "--------------------------------");

            String filename = "C:/Users/m.chami/Desktop/Caisse/Caisse/ticket.txt";
            Process.Start(filename);
        }

        private void ManuelButton_Click(object sender, EventArgs e)
        {
            String filename = "C:/Users/m.chami/Desktop/Caisse/Caisse/aide.pdf";
            Process.Start(filename);
        }

        private void num1_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "1" ;
        }

        private void num2_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "2";
        }

        private void num3_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "3";
        }

        private void num4_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "4";
        }

        private void num5_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "5";
        }

        private void num6_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "6";
        }

        private void num7_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "7";
        }

        private void num8_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "8";
        }

        private void num9_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "9";
        }

        private void num0_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += "0";
        }

        private void virgule_Click(object sender, EventArgs e)
        {
            poidsTextBox.Text += ",";
        }

        private void eff_Click(object sender, EventArgs e)
        {
            poidsTextBox.Clear();
        }

        private void histo_Click(object sender, EventArgs e)
        {
            try
            {
                String filename = "C:/Users/h.batr/source/repos/Caisse/Caisse/bin/Debug/ticket.txt";
                Process.Start(filename);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show("Historique des tickets est supprimé.");
            }
        }
    }
}
