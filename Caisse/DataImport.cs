﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Caisse
{
    class DataImport
    {
        public Dictionary<string, string> ImportProduitPrix(string file)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            try
            {
                using (StreamReader sr = new StreamReader(file))
                {

                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] substrings = line.Split(';');
                        dic.Add(substrings[0], substrings[1]);
                    }
                    sr.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            return dic;
        }
    }
}
