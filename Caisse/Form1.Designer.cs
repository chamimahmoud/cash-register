﻿namespace Caisse
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.NPButton = new System.Windows.Forms.Button();
            this.validerButton = new System.Windows.Forms.Button();
            this.dataButton = new System.Windows.Forms.Button();
            this.voirButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.poidsTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.prixTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.prixGlobalTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.ManuelButton = new System.Windows.Forms.Button();
            this.num1 = new System.Windows.Forms.Button();
            this.num2 = new System.Windows.Forms.Button();
            this.num3 = new System.Windows.Forms.Button();
            this.num4 = new System.Windows.Forms.Button();
            this.num5 = new System.Windows.Forms.Button();
            this.num6 = new System.Windows.Forms.Button();
            this.num7 = new System.Windows.Forms.Button();
            this.num8 = new System.Windows.Forms.Button();
            this.num9 = new System.Windows.Forms.Button();
            this.num0 = new System.Windows.Forms.Button();
            this.virgule = new System.Windows.Forms.Button();
            this.eff = new System.Windows.Forms.Button();
            this.affichageTextBox = new System.Windows.Forms.TextBox();
            this.histo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NPButton
            // 
            this.NPButton.Location = new System.Drawing.Point(35, 33);
            this.NPButton.Name = "NPButton";
            this.NPButton.Size = new System.Drawing.Size(141, 33);
            this.NPButton.TabIndex = 0;
            this.NPButton.Text = "Nouveau Panier";
            this.NPButton.UseVisualStyleBackColor = true;
            this.NPButton.Click += new System.EventHandler(this.NPButton_Click);
            // 
            // validerButton
            // 
            this.validerButton.Location = new System.Drawing.Point(164, 476);
            this.validerButton.Name = "validerButton";
            this.validerButton.Size = new System.Drawing.Size(75, 38);
            this.validerButton.TabIndex = 1;
            this.validerButton.Text = "Valider";
            this.validerButton.UseVisualStyleBackColor = true;
            this.validerButton.Visible = false;
            this.validerButton.Click += new System.EventHandler(this.validerButton_Click);
            // 
            // dataButton
            // 
            this.dataButton.Location = new System.Drawing.Point(218, 33);
            this.dataButton.Name = "dataButton";
            this.dataButton.Size = new System.Drawing.Size(133, 33);
            this.dataButton.TabIndex = 2;
            this.dataButton.Text = "Importer DATA";
            this.dataButton.UseVisualStyleBackColor = true;
            this.dataButton.Click += new System.EventHandler(this.dataButton_Click);
            // 
            // voirButton
            // 
            this.voirButton.Location = new System.Drawing.Point(479, 530);
            this.voirButton.Name = "voirButton";
            this.voirButton.Size = new System.Drawing.Size(107, 34);
            this.voirButton.TabIndex = 3;
            this.voirButton.Text = "Voir Ticket";
            this.voirButton.UseVisualStyleBackColor = true;
            this.voirButton.Visible = false;
            this.voirButton.Click += new System.EventHandler(this.voirButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Indiquier produit";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Induiquer poids ";
            this.label2.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Choisissez votre produit"});
            this.comboBox1.Location = new System.Drawing.Point(173, 90);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(178, 24);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.Visible = false;
            // 
            // poidsTextBox
            // 
            this.poidsTextBox.Location = new System.Drawing.Point(173, 137);
            this.poidsTextBox.Multiline = true;
            this.poidsTextBox.Name = "poidsTextBox";
            this.poidsTextBox.ReadOnly = true;
            this.poidsTextBox.Size = new System.Drawing.Size(143, 22);
            this.poidsTextBox.TabIndex = 9;
            this.poidsTextBox.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(328, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "kg";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(68, 431);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Prix produit";
            this.label4.Visible = false;
            // 
            // prixTextBox
            // 
            this.prixTextBox.Location = new System.Drawing.Point(178, 426);
            this.prixTextBox.Name = "prixTextBox";
            this.prixTextBox.ReadOnly = true;
            this.prixTextBox.Size = new System.Drawing.Size(143, 22);
            this.prixTextBox.TabIndex = 12;
            this.prixTextBox.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(333, 431);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "€";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(378, 453);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Prix global";
            this.label6.Visible = false;
            // 
            // prixGlobalTextBox
            // 
            this.prixGlobalTextBox.Location = new System.Drawing.Point(479, 450);
            this.prixGlobalTextBox.Name = "prixGlobalTextBox";
            this.prixGlobalTextBox.ReadOnly = true;
            this.prixGlobalTextBox.Size = new System.Drawing.Size(121, 22);
            this.prixGlobalTextBox.TabIndex = 15;
            this.prixGlobalTextBox.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(606, 453);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "€";
            this.label7.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // ManuelButton
            // 
            this.ManuelButton.Location = new System.Drawing.Point(490, 33);
            this.ManuelButton.Name = "ManuelButton";
            this.ManuelButton.Size = new System.Drawing.Size(153, 33);
            this.ManuelButton.TabIndex = 17;
            this.ManuelButton.Text = "Manuel d\'utilisation";
            this.ManuelButton.UseVisualStyleBackColor = true;
            this.ManuelButton.Click += new System.EventHandler(this.ManuelButton_Click);
            // 
            // num1
            // 
            this.num1.Location = new System.Drawing.Point(88, 182);
            this.num1.Name = "num1";
            this.num1.Size = new System.Drawing.Size(53, 41);
            this.num1.TabIndex = 18;
            this.num1.Text = "1";
            this.num1.UseVisualStyleBackColor = true;
            this.num1.Visible = false;
            this.num1.Click += new System.EventHandler(this.num1_Click);
            // 
            // num2
            // 
            this.num2.Location = new System.Drawing.Point(178, 182);
            this.num2.Name = "num2";
            this.num2.Size = new System.Drawing.Size(52, 41);
            this.num2.TabIndex = 19;
            this.num2.Text = "2";
            this.num2.UseVisualStyleBackColor = true;
            this.num2.Visible = false;
            this.num2.Click += new System.EventHandler(this.num2_Click);
            // 
            // num3
            // 
            this.num3.Location = new System.Drawing.Point(270, 182);
            this.num3.Name = "num3";
            this.num3.Size = new System.Drawing.Size(49, 43);
            this.num3.TabIndex = 20;
            this.num3.Text = "3";
            this.num3.UseVisualStyleBackColor = true;
            this.num3.Visible = false;
            this.num3.Click += new System.EventHandler(this.num3_Click);
            // 
            // num4
            // 
            this.num4.Location = new System.Drawing.Point(88, 237);
            this.num4.Name = "num4";
            this.num4.Size = new System.Drawing.Size(53, 43);
            this.num4.TabIndex = 21;
            this.num4.Text = "4";
            this.num4.UseVisualStyleBackColor = true;
            this.num4.Visible = false;
            this.num4.Click += new System.EventHandler(this.num4_Click);
            // 
            // num5
            // 
            this.num5.Location = new System.Drawing.Point(178, 237);
            this.num5.Name = "num5";
            this.num5.Size = new System.Drawing.Size(52, 43);
            this.num5.TabIndex = 22;
            this.num5.Text = "5";
            this.num5.UseVisualStyleBackColor = true;
            this.num5.Visible = false;
            this.num5.Click += new System.EventHandler(this.num5_Click);
            // 
            // num6
            // 
            this.num6.Location = new System.Drawing.Point(270, 237);
            this.num6.Name = "num6";
            this.num6.Size = new System.Drawing.Size(49, 43);
            this.num6.TabIndex = 23;
            this.num6.Text = "6";
            this.num6.UseVisualStyleBackColor = true;
            this.num6.Visible = false;
            this.num6.Click += new System.EventHandler(this.num6_Click);
            // 
            // num7
            // 
            this.num7.Location = new System.Drawing.Point(87, 296);
            this.num7.Name = "num7";
            this.num7.Size = new System.Drawing.Size(54, 44);
            this.num7.TabIndex = 24;
            this.num7.Text = "7";
            this.num7.UseVisualStyleBackColor = true;
            this.num7.Visible = false;
            this.num7.Click += new System.EventHandler(this.num7_Click);
            // 
            // num8
            // 
            this.num8.Location = new System.Drawing.Point(178, 296);
            this.num8.Name = "num8";
            this.num8.Size = new System.Drawing.Size(52, 45);
            this.num8.TabIndex = 25;
            this.num8.Text = "8";
            this.num8.UseVisualStyleBackColor = true;
            this.num8.Visible = false;
            this.num8.Click += new System.EventHandler(this.num8_Click);
            // 
            // num9
            // 
            this.num9.Location = new System.Drawing.Point(270, 297);
            this.num9.Name = "num9";
            this.num9.Size = new System.Drawing.Size(49, 44);
            this.num9.TabIndex = 26;
            this.num9.Text = "9";
            this.num9.UseVisualStyleBackColor = true;
            this.num9.Visible = false;
            this.num9.Click += new System.EventHandler(this.num9_Click);
            // 
            // num0
            // 
            this.num0.Location = new System.Drawing.Point(178, 361);
            this.num0.Name = "num0";
            this.num0.Size = new System.Drawing.Size(52, 43);
            this.num0.TabIndex = 27;
            this.num0.Text = "0";
            this.num0.UseVisualStyleBackColor = true;
            this.num0.Visible = false;
            this.num0.Click += new System.EventHandler(this.num0_Click);
            // 
            // virgule
            // 
            this.virgule.Location = new System.Drawing.Point(270, 361);
            this.virgule.Name = "virgule";
            this.virgule.Size = new System.Drawing.Size(49, 43);
            this.virgule.TabIndex = 29;
            this.virgule.Text = ",";
            this.virgule.UseVisualStyleBackColor = true;
            this.virgule.Visible = false;
            this.virgule.Click += new System.EventHandler(this.virgule_Click);
            // 
            // eff
            // 
            this.eff.Location = new System.Drawing.Point(50, 361);
            this.eff.Name = "eff";
            this.eff.Size = new System.Drawing.Size(93, 43);
            this.eff.TabIndex = 30;
            this.eff.Text = "Effacer";
            this.eff.UseVisualStyleBackColor = true;
            this.eff.Visible = false;
            this.eff.Click += new System.EventHandler(this.eff_Click);
            // 
            // affichageTextBox
            // 
            this.affichageTextBox.Location = new System.Drawing.Point(410, 93);
            this.affichageTextBox.Multiline = true;
            this.affichageTextBox.Name = "affichageTextBox";
            this.affichageTextBox.Size = new System.Drawing.Size(212, 335);
            this.affichageTextBox.TabIndex = 31;
            this.affichageTextBox.Visible = false;
            // 
            // histo
            // 
            this.histo.Location = new System.Drawing.Point(12, 520);
            this.histo.Name = "histo";
            this.histo.Size = new System.Drawing.Size(107, 44);
            this.histo.TabIndex = 32;
            this.histo.Text = "Historique des tickets";
            this.histo.UseVisualStyleBackColor = true;
            this.histo.Click += new System.EventHandler(this.histo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 576);
            this.Controls.Add(this.histo);
            this.Controls.Add(this.affichageTextBox);
            this.Controls.Add(this.eff);
            this.Controls.Add(this.virgule);
            this.Controls.Add(this.num0);
            this.Controls.Add(this.num9);
            this.Controls.Add(this.num8);
            this.Controls.Add(this.num7);
            this.Controls.Add(this.num6);
            this.Controls.Add(this.num5);
            this.Controls.Add(this.num4);
            this.Controls.Add(this.num3);
            this.Controls.Add(this.num2);
            this.Controls.Add(this.num1);
            this.Controls.Add(this.ManuelButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.prixGlobalTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.prixTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.poidsTextBox);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.voirButton);
            this.Controls.Add(this.dataButton);
            this.Controls.Add(this.validerButton);
            this.Controls.Add(this.NPButton);
            this.Name = "Form1";
            this.Text = " ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button NPButton;
        private System.Windows.Forms.Button validerButton;
        private System.Windows.Forms.Button dataButton;
        private System.Windows.Forms.Button voirButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox poidsTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox prixTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox prixGlobalTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.Button ManuelButton;
        private System.Windows.Forms.Button num1;
        private System.Windows.Forms.Button num2;
        private System.Windows.Forms.Button num3;
        private System.Windows.Forms.Button num4;
        private System.Windows.Forms.Button num5;
        private System.Windows.Forms.Button num6;
        private System.Windows.Forms.Button num7;
        private System.Windows.Forms.Button num8;
        private System.Windows.Forms.Button num9;
        private System.Windows.Forms.Button num0;
        private System.Windows.Forms.Button virgule;
        private System.Windows.Forms.Button eff;
        private System.Windows.Forms.TextBox affichageTextBox;
        private System.Windows.Forms.Button histo;
    }
}

