﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Caisse
{
    public class ticket
    {
        private string filepath = "Ticket.txt";

        public string Filepath
        {
            get => default;
            set
            {
                filepath = value;
            }
        }

        public void Log(string message)
        {
            using (StreamWriter sw = new StreamWriter(filepath, true))
            {
                sw.WriteLine(message);
            }
        }
    }
}